import fs from "node:fs";
import path from "node:path";
import { DirListing } from "./types";
import ffmpegPath from "@ffmpeg-installer/ffmpeg";
import ffmpeg from "fluent-ffmpeg";

ffmpeg.setFfmpegPath(ffmpegPath.path);

export const MEDIA_DIR = "courses";
export const THUMBNAIL_DIR = "thumbnails";
export const PORT = 8333;

// TODO: ditch custom type in favor of fs.Dirent
export const pathToTree = (dPath: string): DirListing => {
  const dir = new DirListing();
  dir.name = path.basename(dPath);
  dir.type = "file";

  if (fs.existsSync(dPath) && fs.lstatSync(dPath).isDirectory() && dir.name != 'app') {
    dir.type = "directory";
    fs.readdirSync(dPath).forEach((f) => {
      dir.children.push(pathToTree(path.join(dPath, f)));
    });
  }

  return dir;
};

export function stripPrefix(str: string): string {
  return str.replace(/^[\d\W]+/, "");
}

// write first frame of video at fPath to thumbnail directory
export const createVideoThumbnail = async (fPath: string): Promise<string> => {
  if (!fs.existsSync(fPath)) {
    return "Error: " + fPath + " not found";
  }

  let tPath = path.normalize(
    path.join(
      THUMBNAIL_DIR,
      fPath.slice(fPath.indexOf(path.sep), fPath.length) + ".png"
    )
  );

  const tPathDir = path.dirname(tPath);

  // create missing parent directories
  if (!fs.existsSync(tPathDir)) {
    fs.mkdirSync(path.dirname(tPath), { recursive: true });
  }

  return new Promise((resolve, reject) => {
    ffmpeg(fPath)
      .seekInput(1)
      .frames(1)
      .noAudio()
      .size("640x?")
      .save(tPath)
      .on("error", (e) => reject(e))
      .on("end", () => {
        resolve(tPath);
      });
  });
};

export const getVideoDuration = async (fPath: string): Promise<number> => {
  let fPathSafe = path.normalize(fPath);

  return new Promise((resolve, reject) => {
    ffmpeg.ffprobe(fPathSafe, (err, metadata) => {
      if (err) {
        reject(err);
      } else {
        resolve(Math.round(metadata.format.duration));
      }
    });
  });
};

export function toTitleCase(s: string) {
  return s.replace(
    /\b\w+('\w{1})?/g,
    (txt: string) => {
      return txt.charAt(0).toUpperCase() + txt.substring(1).toLowerCase();
    }
  );
}