import path from "node:path"
import { eq, and, isNotNull, asc, sql } from "drizzle-orm";
import { drizzle, BunSQLiteDatabase } from "drizzle-orm/bun-sqlite";
import { Database } from "bun:sqlite";
import { Chapter, Course, Settings, Video } from "./types";
import { BunFile } from "bun";
import { MEDIA_DIR, createVideoThumbnail, getVideoDuration, stripPrefix, toTitleCase } from "./util";
import { migrate } from 'drizzle-orm/bun-sqlite/migrator'
import * as schema from "./schema";

const sqlite: Database = new Database("db/ucourse.db", { create: true });
const db: BunSQLiteDatabase<typeof schema> = drizzle(sqlite, { schema });

// create tables if they don't exist
migrate(db, {migrationsFolder: 'db/drizzle'});

export async function getSettings() : Promise<Settings> {
  return (await db.select().from(schema.settings))[0]
}

export async function setSettings(s: Settings) {
  await db.update(schema.settings)
    .set({
      background: s.background,
      coursesDir: s.coursesDir,
      thumbnailDir: s.thumbnailDir,
      darkMode: s.darkMode,
      autoPlay: s.autoPlay
    })
}

export async function getCourseProgress(courseID: string) {
  const courseIDint: number = parseInt(courseID);

  if(Number.isNaN(courseIDint))  return {}

  const prog = await db.select({
      courseID: schema.videos.courseID,
      numVideos: sql<number>`count(${schema.videos.id})`,
      numChapters: sql<number>`count(distinct ${schema.videos.chapterID})`,
      totalRuntime: sql<number>`sum(${schema.videos.lengthSec})`,
      totalWatchtime: sql<number>`sum(${schema.videos.watchedSec})`
    })
    .from(schema.videos)
    .groupBy(schema.videos.courseID)
    .where(eq(schema.videos.courseID, courseIDint))

  if(prog.length < 1) return {};
  return prog[0];
}

export async function setVideoWatchedSec(videoID: number, n: number) {  
  await db.update(schema.videos)
    .set({ watchedSec: n })
    .where(eq(schema.videos.id, videoID))
}

export async function getCourses() : Promise<Course[]> {
  const c = await db.query.courses.findMany({
    orderBy: [asc(schema.courses.id)]
  });

  return c
}

export async function getCourse(id: string) : Promise<Course> {
  const courseID: number = parseInt(id);

  const crs: Course[] = await db.query.courses.findMany({
    where: eq(schema.courses.id, courseID),
    with: {
      chapters: {
        orderBy: [asc(schema.chapters.sortIndex)],
        with: {
            videos: {
                orderBy: [
                    asc(schema.videos.chapterID), 
                    asc(schema.videos.sortIndex)
    ],}}}},});                                          // l o l

  if(crs.length < 1)    return new Course()

  return crs[0]
}

export async function getVideo(id: string) : Promise<Video> {
    const videoID: number = parseInt(id)

    const vid: Video[] = await db.select()
        .from(schema.videos)
        .where(isNotNull(schema.videos.filePath))
        .where(eq(schema.videos.id, videoID))
        .limit(1)

    if(vid.length < 1)    return new Video()

    return vid[0]
}

export async function getCourseThumbnail(id: string) : Promise<BunFile> {
  const courseID: number = parseInt(id)

  const crs: Course[] = await db.select()
      .from(schema.courses)
      .where(isNotNull(schema.courses.thumbnailPath))
      .where(eq(schema.courses.id, courseID))
      .limit(1)

   if(crs.length < 1)    return Bun.file('')

  return Bun.file(crs[0].thumbnailPath)
}

export async function deleteCourse(id: string) {
  const courseID: number = parseInt(id);

  await db.delete(schema.videos)
    .where(eq(schema.videos.courseID, courseID))

  await db.delete(schema.chapters)
    .where(eq(schema.chapters.courseID, courseID))

  await db.delete(schema.courses)
    .where(eq(schema.courses.id, courseID))
}

export async function updateCourse(c: Course) {
  await db.update(schema.courses)
    .set({
      title: c.title,
      description: c.description,
    })
    .where(eq(schema.courses.id, c.id))
}

export async function updateCourseTitles(courseID: number, delimiter: string) {
  await db.update(schema.chapters)
    .set({
      title: sql`replace(${schema.chapters.title}, ${delimiter}, ' ')`
    })
    .where(eq(schema.chapters.courseID, courseID))

  await db.update(schema.videos)
    .set({
      title: sql`replace(${schema.videos.title}, ${delimiter}, ' ')`
    })
    .where(eq(schema.videos.courseID, courseID))
}

export async function createCourse(c: Course) : Promise<Course> {
  return new Promise(async (resolve, reject) => {
  let rawCrs: Course[] = await db.insert(schema.courses)
    .values({title: c.title, description: c.description})
    .returning()


  if(rawCrs.length < 1)    return new Course()

  let newCrs: Course = rawCrs[0]

  // insert chapters
  c.chapters.forEach(async (chap, chapIdx) => {
    const rawChap: Chapter[] = await db.insert(schema.chapters)
      .values({
        title: stripPrefix(chap.title),
        courseID: newCrs.id,
        sortIndex: chapIdx
      })
      .returning()

    const newChap: Chapter = (rawChap.length < 1 ? new Chapter() : rawChap[0])

    // insert videos
    chap.videos.forEach(async (vid, vidIdx) => {
      vid.filePath = path.join(MEDIA_DIR, vid.filePath);

      // generate course thumbnail if not existing
      if(c.thumbnailPath === undefined || c.thumbnailPath == "") {
        c.thumbnailPath = " ";
        c.thumbnailPath = await createVideoThumbnail(vid.filePath)
        
        const res = (await db.update(schema.courses)
          .set({ thumbnailPath: c.thumbnailPath })
          .where(eq(schema.courses.id, newCrs.id))
          .returning())[0]

        resolve(res);
      }

      vid.lengthSec = await getVideoDuration(vid.filePath);

      await db.insert(schema.videos)
        .values({
          title: stripPrefix(vid.title),
          sortIndex: vidIdx,
          filePath: vid.filePath,
          lengthSec: vid.lengthSec || 0,
          watchedSec: 0,
          courseID: newCrs.id,
          chapterID: newChap.id
        })
    })
  })
})
}

