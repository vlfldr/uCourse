import { relations } from 'drizzle-orm';
import { sqliteTable, text, integer } from 'drizzle-orm/sqlite-core';

export const courses = sqliteTable('courses', {
    id: integer('id').primaryKey(),
    title: text('title').notNull(),
    description: text('description'),
    thumbnailPath: text('thumbnailPath')
});

export const coursesRelations = relations(courses, ({ many }) => ({
    chapters: many(chapters)
}));

export const chapters = sqliteTable('chapters', {
    id: integer('id').primaryKey(),
    title: text('title').notNull(),
    sortIndex: integer('sortIndex'),

    courseID: integer('courseID').references(() => courses.id),
});

export const chaptersRelations = relations(chapters, ({ one, many }) => ({
    course: one(courses, {
        fields: [chapters.courseID],
        references: [courses.id]
    }),
    videos: many(videos)
}));

export const videos = sqliteTable('videos', {
    id: integer('id').primaryKey(),
    title: text('title').notNull(),
    sortIndex: integer('sortIndex').notNull(),
    filePath: text('filePath'),
    lengthSec: integer('lengthSec'),
    watchedSec: integer('watchedSec'),
    
    courseID: integer('courseID').notNull().references(() => courses.id),
    chapterID: integer('chapterID').notNull().references(() => chapters.id),
});

export const videosRelations = relations(videos, ({ one }) => ({
    course: one(courses, {
        fields: [videos.courseID],
        references: [courses.id]
    }),
    chapter: one(chapters, {
        fields: [videos.chapterID],
        references: [chapters.id]
    })
}));

export const settings = sqliteTable('settings', {
    background: text('background'),
    coursesDir: text('coursesDir'),
    thumbnailDir: text('thumbnailDir'),
    darkMode: text('darkMode'),
    autoPlay: integer('autoPlay')
});