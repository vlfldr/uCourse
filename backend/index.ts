import { BunFile } from "bun";
import { Hono } from "hono";
import { cors } from "hono/cors";
import { Course, Settings } from "./src/types.ts";
import { pathToTree, MEDIA_DIR, PORT } from "./src/util.ts";
import {
  getCourse,
  getCourses,
  getVideo,
  getCourseThumbnail,
  createCourse,
  getSettings,
  setSettings,
  setVideoWatchedSec,
  getCourseProgress,
  updateCourse,
  deleteCourse,
  updateCourseTitles,
} from "./src/crud.ts";

import fs from "node:fs";

const app = new Hono();
app.use("*", cors());

export default {
  port: PORT,
  hostname: "0.0.0.0",
  fetch: app.fetch,
};

app.get("/", (c) => c.redirect("/courses"));
app.get("/courses", async (c) => {
  const courses = await getCourses();
  return c.json({ courses }.courses);
});

app.get("/video/:id/stream", async (c) => {
  const vid = await getVideo(c.req.param("id"));
  const vidSize = fs.statSync(vid.filePath).size;

  // check for range header
  const range = c.req.header("range");
  if (!range)
    return new Response("400: Requires range header", { status: 400 });

  let parts = range.split("=")[1];
  let start = Number(parts.split("-")[0]);
  let end = Number(parts.split("-")[1]);

  if (!end) {
    const CHUNK_SIZE = 10 ** 6;
    end = Math.min(start + CHUNK_SIZE, vidSize - 1);
  }

  const contentLength = end - start + 1;

  c.res.headers.append("Content-Range", `bytes ${start}-${end}/${vidSize}`);
  c.res.headers.append("Accept-Ranges", "bytes");
  c.res.headers.append("Content-Length", contentLength.toString());
  c.res.headers.append("Content-Type", "video/mp4");

  const f = Bun.file(vid.filePath);

  if (start === end) return new Response(f);

  //let a = new Response(f.slice(start, end), {status: 206, headers: c.res.headers});

  return new Response(f.slice(start, end))
});

app.get("/course/:id", async (c) => {
  const course = await getCourse(c.req.param("id"));
  return c.json({ course }.course);
});

app.get("/course/:id/progress", async (c) => {
  const prog = await getCourseProgress(c.req.param("id"));

  return c.json({ prog }.prog);
});

app.get("/course/:id/thumbnail", async (c) => {
  const f: BunFile = await getCourseThumbnail(c.req.param("id"));

  if (f.size === 0) return new Response("404");

  return new Response(f);
});

app.post("/course/create", async (c) => {
  const crs: Course = await c.req.json();

  const newCrs: Course = await createCourse(crs);

  return c.json({ newCrs }.newCrs);
});

app.delete("/course/:id/delete", async (c) => {
  const courseID = c.req.param("id");

  try {
    await deleteCourse(courseID);
  } catch (e) {
    return new Response(JSON.stringify({ Status: "Error " + e }));
  }

  return new Response(JSON.stringify({ Status: "Success" }));
});

app.post("/course/update", async (c) => {
  const crs: Course = await c.req.json();

  try {
    await updateCourse(crs);
  } catch (e) {
    return new Response(JSON.stringify({ Status: "Error " + e }));
  }

  return new Response(JSON.stringify({ Status: "Success" }));
});

app.post("/course/:id/update/titles", async (c) => {
  const courseID: number = parseInt(c.req.param("id"));
  const delimiter: string = (await c.req.json())["delimiter"];

  try {
    await updateCourseTitles(courseID, delimiter);
  } catch (e) {
    return new Response(JSON.stringify({ Status: "Error " + e }));
  }

  return new Response(JSON.stringify({ Status: "Success" }));
});

app.post("/video/update/watchedSec", async (c) => {
  const raw = await c.req.json();
  const videoIDs: number[] = raw['videoIDs']
  const watchedSecs: number[] = raw['watchedSecs']

  try {
    for (let i = 0; i < videoIDs.length; i++) {
      await setVideoWatchedSec(videoIDs[i], watchedSecs[i]);
    }
  } catch (e) {
    return new Response(JSON.stringify({ Status: "Error " + e }));
  }

  return new Response(JSON.stringify({ Status: "Success" }));
});

app.get("/files/list", (c) => {
  return new Response(JSON.stringify(pathToTree(MEDIA_DIR)));
});

app.get("/settings", async () => {
  return new Response(JSON.stringify(await getSettings()));
});

app.post("/settings/update", async (c) => {
  const data: Settings = await c.req.json();

  try {
    await setSettings(data);
  } catch (e) {
    return new Response(JSON.stringify({ Status: "Error " + e }));
  }

  return new Response(JSON.stringify({ Status: "Success" }));
});
