CREATE TABLE `chapters` (
	`id` integer PRIMARY KEY NOT NULL,
	`title` text NOT NULL,
	`sortIndex` integer,
	`courseID` integer,
	FOREIGN KEY (`courseID`) REFERENCES `courses`(`id`) ON UPDATE no action ON DELETE no action
);
--> statement-breakpoint
CREATE TABLE `courses` (
	`id` integer PRIMARY KEY NOT NULL,
	`title` text NOT NULL,
	`description` text,
	`thumbnailPath` text
);
--> statement-breakpoint
CREATE TABLE `settings` (
	`background` text,
	`coursesDir` text,
	`thumbnailDir` text,
	`darkMode` text,
	`autoPlay` integer
);
--> statement-breakpoint
CREATE TABLE `videos` (
	`id` integer PRIMARY KEY NOT NULL,
	`title` text NOT NULL,
	`sortIndex` integer NOT NULL,
	`filePath` text,
	`lengthSec` integer,
	`watchedSec` integer,
	`courseID` integer NOT NULL,
	`chapterID` integer NOT NULL,
	FOREIGN KEY (`courseID`) REFERENCES `courses`(`id`) ON UPDATE no action ON DELETE no action,
	FOREIGN KEY (`chapterID`) REFERENCES `chapters`(`id`) ON UPDATE no action ON DELETE no action
);
--> statement-breakpoint
INSERT INTO `settings` VALUES ("Circuits", "courses", "thumbnails", "light", 1)