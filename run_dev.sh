#! /bin/sh

echo ""

# check for bun. prompt to download if not installed
if ! which bun &> /dev/null ; then
    echo "Bun is required to run uCourse."
    read -p "Would you like to download and install it? [y/n] " answer

    if ["$answer" =~ ^[Yy]$]; then
        curl -fsSL https://bun.sh/install | bash
    else
        echo "Goodbye."
        exit 1;
    fi
fi

# check for ffmpeg. exit if not installed
if ! which ffprobe &> /dev/null ; then
    echo "ffmpeg is required to run uCourse."
    echo "Download and install with 'apt install ffmpeg', 'brew install ffmpeg', etc."
    echo "See also: https://ffmpeg.org/download.html"
    echo ""
    
    exit 1;
fi

cd frontend && bun i && bun --bun run dev &
cd backend  && bun i && bun run --hot index.ts

kill $!