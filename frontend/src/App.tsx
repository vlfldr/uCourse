import {
  createResource,
  createSignal,
  lazy,
} from "solid-js";
import {
  Routes,
  Route
} from "@solidjs/router";
import Navbar from "./components/Navbar";
import CourseList from "./components/CourseList";
import { getAllCourses } from "./api";

const Theater = lazy(() => import("./components/Theater"));
const SettingsModal = lazy(() => import("./components/SettingsModal"));
const CourseEditor = lazy(() => import("./components/CourseEditor"));

const App = () => {
  const [courses, {mutate, refetch}] = createResource(getAllCourses);
  const [editorCourseId, setEditorCourseId] = createSignal("-1");

  return (
    <>
      <Navbar />
      <SettingsModal />
      <CourseEditor 
        courseID={editorCourseId}
        setCourseId={setEditorCourseId} 
        refetch={refetch} />
 
      <Routes>
        <Route 
          path="/"
          element={<CourseList courses={courses} 
          fetchCourses={refetch}
          setEditorCourseId={setEditorCourseId} />} />
        <Route 
          path="/watch/:id"
          element={<Theater />} />
      </Routes>
    </>
  );
};

export default App;