import { DirListing, Course, Video, Chapter } from "./types";
import settings, { updateSettings } from "./settings";

export const toggleModal = (modalTitle: string, state?: boolean) => {
  const modal = (document.getElementById(modalTitle + "Modal") as HTMLDialogElement);
  if(!state) {
    state = !modal.open;
  }

  state === true ? modal.showModal() : modal.close();
}

export const sortVideos = (arr: Video[]): Video[] => {
  return arr.sort((a, b) => {
    const aPrefix = parseInt(a.title.split(/([0-9]+)/)[1]);
    const bPrefix = parseInt(b.title.split(/([0-9]+)/)[1]);

    if(isNaN(aPrefix) || isNaN(bPrefix))  return 0;

    if (aPrefix < bPrefix) {
      return -1;
    } else if (aPrefix > bPrefix) {
      return 1;
    } else {
      return 0;
    }
  });
}

export const sortChapters = (arr: Chapter[]): Chapter[] => {
    return arr.sort((a, b) => {
      const aPrefix = parseInt(a.title.split(/([0-9]+)/)[1]);
      const bPrefix = parseInt(b.title.split(/([0-9]+)/)[1]);
      
      if(isNaN(aPrefix) || isNaN(bPrefix))  return 0;

      if (aPrefix < bPrefix) {
        return -1;
      } else if (aPrefix > bPrefix) {
        return 1;
      } else {
        return 0;
      }
    });
  }
export const setSysDarkMode = (isDark: boolean) => {
  const r = document.getElementById('root')!
  const darkStr = isDark ? "dark" : "light";
  const notDarkStr = isDark ? "light" : "dark";

  localStorage.theme = darkStr;
  
  if(isDark) {
    document.documentElement.classList.add("dark");
  } else {
    document.documentElement.classList.remove("dark");
  }

  r.classList.add('bg-pattern-intersections')
  r.classList.remove('bg-pattern-' + settings.background + '-' + notDarkStr);
  r.classList.add('bg-pattern-' + settings.background + '-' + darkStr);

  settings.darkMode = darkStr;
  updateSettings();
}

export const getSysDarkMode = () => {
  return settings.darkMode === 'dark' || localStorage.theme === 'dark' 
    || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)
}

export function stripPrefix(str: string): string {
    return str.replace(/^[\d\W]+/, '');
}

export const findVideoFiles = (
  c: Course,
  dir: DirListing,
  vids: Video[],
  depth: number
): Video[] => {
  if (dir.type == "file") {
    if (dir.name.endsWith(".mp4")) {
      let newVid: Video = new Video();

      newVid.title = toTitleCase(dir.name.split("/").pop()!.slice(0, -4));
      newVid.sortIndex = vids.length;
      newVid.filePath = dir.name;

      vids.push(newVid);
      return vids;
    }
  }

  if (depth < 2 && dir.type == "directory" && dir.children) {
    dir.children.sort((a, b) =>
      a.name.toLowerCase().localeCompare(b.name.toLowerCase())
    );
    dir.children!.forEach((f) => {
      appendToFilepath(f, dir.name);
      vids.concat(findVideoFiles(c, f, vids, depth + 1));
    });
  }

  return vids;
};

const appendToFilepath = (d: DirListing, prefix: string): DirListing => {
  d.name = prefix + "/" + d.name; // paths are normalized for underlying OS on backend
  return d;
};

export const dirListingToCourse = (d: DirListing, usesChapters: boolean): Course => {
  let chapters: Chapter[] = [];
  let videos: Video[] = [];
  let c: Course = new Course();

  const singleChap = new Chapter();
  singleChap.title = d.name;

  c.title = d.name;
  c.chapters = usesChapters ? [] : [singleChap];
  c.videos = videos;

  if(!d.children) return c;

  if(usesChapters) {
    d.children.sort((a, b) =>
      a.name.toLowerCase().localeCompare(b.name.toLowerCase())
    );

    d.children.forEach((ch, i) => {
      if (ch.type != "file") {
        let chap: Chapter = new Chapter();
        chap.sortIndex = i;
        chap.title = toTitleCase(ch.name);
        appendToFilepath(ch, d.name);
        findVideoFiles(c, ch, chap.videos, 0);
        chap.videos = sortVideos(chap.videos);
        c.videos = c.videos.concat(chap.videos);

        chapters.push(chap);
      }
  })
  c.chapters = sortChapters(chapters);
  } 
  
  else {
    findVideoFiles(c, d, c.chapters[0].videos, 0);
    c.chapters[0].videos = sortVideos(c.chapters[0].videos);
    c.videos = c.chapters[0].videos;
  }

  return c;
};

export function toTitleCase(s: string) {
  return s.replace(
    /\b\w+('\w{1})?/g,
    (txt: string) => {
      return txt.charAt(0).toUpperCase() + txt.substring(1).toLowerCase();
    }
  );
}
