export class Course {
    id: number            = -1;
    title: string         = '';
    description: string   = '';
    thumbnailPath: string = '';

    chapters: Chapter[]   = [];
    videos: Video[]       = [];
}

export type Settings = {
  background: string,
  coursesDir: string,
  thumbnailDir: string,
  darkMode: string,
  autoPlay: number
}
  
export class Video {
  id: number          = -1;
  title: string       = '';
  sortIndex: number   = 0;
  description: string = '';
  filePath: string    = '';
  // thumbnailPath: string = '';
  // blurhash: string    = '';
  lengthSec: number   = 0;
  watchedSec: number  = 0;

  courseID: number    = 0;
  course?: Course     = undefined;
  chapterID: number   = 0;
  chapter?: Chapter   = undefined;
}

export class Chapter {
  id: number        = -1;
  sortIndex: number = 0;
  title: string     = '';

  courseID: number  = 0;
  course?: Course   = undefined;

  videos: Video[]   = [];
}

export class DirListing {
  name: string  = '';
  type: string  = '';
  children?: DirListing[]  = [];
}