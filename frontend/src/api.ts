import { Course, DirListing, Settings } from './types';

export const apiURL = '/api/'
//export const apiURL = 'http://localhost:8333/';

export async function getAllCourses(): Promise<Course[]> {
    const response = await fetch(apiURL + 'courses');
    const courses: Course[] = await response.json();

    return courses;
}

export async function listMediaDir(): Promise<DirListing> {
    const response = await fetch(apiURL + 'files/list');

    return await response.json();
}

export async function updateVideoWatchedSec(videoIDs: number[], watchedSecs: number[]) {
    const response = await fetch (apiURL + 'video/update/watchedSec', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'         
        },
        body: JSON.stringify({
            "videoIDs": videoIDs,
            "watchedSecs": watchedSecs.map(ws => Math.floor(ws)) 
        })
    });

    return await response.json();
}

export async function createCourse(c: Course): Promise<Course> {
    const response = await fetch(apiURL + 'course/create', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'         
        },
        body: JSON.stringify(c)
    });

    return await response.json();
}

export async function deleteCourse(id: string | number) {
    await fetch(apiURL + 'course/' + id + '/delete', 
        {method: 'DELETE'})
}

export async function updateCourse(c: Course): Promise<Course> {
    const response = await fetch(apiURL + 'course/update', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'         
        },
        body: JSON.stringify(c)
    });

    return await response.json();
}

export async function updateCourseTitles(courseID: number | string, delimiter: string): Promise<Course> {
    const response = await fetch(apiURL + 'course/' + courseID + '/update/titles', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'         
        },
        body: JSON.stringify({"delimiter": delimiter})
    });

    return await response.json();
}

export async function getFullCourse(courseID: string): Promise<Course | undefined> {
    if(courseID == "-1") return undefined;
    const response = await fetch(apiURL + 'course/' + courseID);

    let crs = await response.json();
    return crs;
}

export async function getCourseProgress(c: Course): Promise<{numVideos: number, numChapters: number, totalRuntime: number, totalWatchtime: number}> {
    const response = await fetch (apiURL + 'course/' + c.id.toString() + '/progress');

    let prog = await response.json();
    return prog;
}

export async function getCourseThumbnail(c: Course): Promise<string> {
    const response = await fetch(apiURL + 'course/' + c.id + '/thumbnail');
    const img: Blob = await response.blob();

    return URL.createObjectURL(img);
}

export async function getSettings(): Promise<Settings> {
    const response = await fetch(apiURL + 'settings')

    const stg = await response.json();
    return stg;
}

export async function setSettings(c: Settings): Promise<boolean> {
    const response = await fetch (apiURL + 'settings/update', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'         
        },
        body: JSON.stringify(c)
    });

    const success = await response.json();
    return success;
}

// DEVELOPMENT ONLY. drops and recreates all tables
export async function dropAllTables(): Promise<string> {
    const response = await fetch(apiURL + 'DEV_ONLY_droptables', 
        {method: 'DELETE'});
    const status = await response.json();

    return status.message;
}