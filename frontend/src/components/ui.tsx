import {
  Accessor,
  Setter,
  onMount,
  JSXElement,
  Show,
  Switch,
  Match,
  createMemo
} from "solid-js";
import { Video } from "../types";
import {
  FaRegularCircle,
  FaSolidCircleHalfStroke,
  FaSolidCircleCheck,
} from "solid-icons/fa";

type CheckboxProps = {
  checked: Accessor<boolean>;
  setChecked: Setter<boolean>;
  label: string;
  reverse?: boolean;
};

export const UICheckbox = (props: CheckboxProps) => {
  
  return (
    <div class="form-control">
      <label class="label cursor-pointer">
      <Show when={props.reverse}>
          <span class="label-text text-black dark:text-white">{props.label}</span>
        </Show>
        <input type="checkbox" class="toggle toggle-md toggle-accent" 
          checked={props.checked()} 
          onClick={() => props.setChecked(!props.checked())}
        />
        <Show when={!props.reverse}>
          <span class="label-text ml-5 text-black dark:text-white">{props.label}</span>
        </Show>
      </label>
    </div>
  );
};

type IconButtonProps = {
  icon: JSXElement;
  onClick?: (e: Event) => void;
};

export const UIIconButton = (props: IconButtonProps) => {
  return (
    <button
      type="button"
      onClick={props.onClick}
      aria-label={props.icon?.toString()}
      class="btn btn-square bg-transparent border-none hover:bg-transparent hover:text-space dark:hover:text-white"
    >
      {props.icon}
    </button>
  );
};

type FloatingIconButtonProps = {
  icon: JSXElement;
  onClick?: (e: Event) => void;
  title: string;
};

export const UIFloatingIconButton = (props: FloatingIconButtonProps) => {
  return (
    <div
    onClick={props.onClick}
    title={props.title}
      class="fixed bottom-10 cursor-pointer right-10 z-8 flex h-14 w-14 items-center justify-end
      shadow-md hover:shadow-lg active:shadow-sm transition-all duration-200 ease-in-out
      rounded-lg bg-violet hover:bg-violet-700 uppercase text-white hover:w-[8.4rem] overflow-hidden"
    >
      <div
        class="rounded-full flex justify-center items-center gap-5 min-w-[8.4rem]"
      >
        <p>Import</p>{props.icon}
      </div>
    </div>
  );
};

type ButtonProps = {
  text: string;
  type?: "primary" | "secondary" | "tertiary";
  enabled?: boolean;
  onClick?: (e: Event) => void;
};

export const UIButton = (props: ButtonProps) => {
  onMount(() => {
    if (props.type == undefined) {
      props.type = "primary";
    }

    if (props.enabled == undefined) {
      props.enabled = true;
    }
  });

  if (props.type == "primary") {
    return (
      <Show
        when={props.enabled}
        fallback={
          <button type="button" class="btn btn-primary ring-none outline-none border-none !text-neutral-400 !hover:text-neutral-300 bg-violet hover:bg-violet-700" disabled>
            {props.text}
          </button>
        }
      >
        <button type="button" onClick={props.onClick} class="btn btn-primary ring-none outline-none border-none bg-violet hover:bg-violet-700">
          {props.text}
        </button>
      </Show>
    );
  } else if (props.type == "secondary") {
    return (
      <Show
        when={props.enabled}
        fallback={
          <button type="button" class="btn btn-neutral" disabled>
            {props.text}
          </button>
        }
      >
        <button type="button" class="btn btn-neutral bg-space-300 !hover:text-white hover:bg-space-600 ring-none outline-none border-none " onClick={props.onClick}>
          {props.text}
        </button>
      </Show>
    );
  } else {
    return (
      <button type="button" onClick={props.onClick} class="btn btn-ghost">
        {props.text}
      </button>
    );
  }
};

export const UISpinner = () => {
  return (
    <div class="flex m-10 flex-wrap justify-center items-center gap-10 h-full z-13">
      <span class="loading loading-dots loading-lg"></span>
    </div>
  );
};