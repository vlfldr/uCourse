import { Accessor, For, Show, createSignal } from "solid-js";
import { Course } from "../types";
import { stripPrefix } from "../util";
import { FaSolidChevronDown, FaSolidChevronUp, FaSolidCircleInfo, FaSolidInfo, FaSolidTriangleExclamation } from "solid-icons/fa";

type ImporterAccordionProps = {
  course: Accessor<Course>;
};

const ImporterAccordion = (props: ImporterAccordionProps) => {
  const isHidden =
    props.course().chapters && props.course().chapters.length !== 1;

  return (
    <div
      class="overflow-y-scroll max-h-[28rem] mb-5 rounded-md 
    bg-neutral-300 dark:bg-neutral-600 text-black dark:text-white"
    >
      <Show when={props.course().chapters.length === 0}>
        <div
          class="bg-red cursor-pointer rounded-md m-5 p-2.5 px-5 shadow-md flex flex-col items-center"
        >
          <div class="text-xl font-medium flex justify-between w-full items-center m-2.5">
            <p>No chapter folders found. Try going back and toggling the checkbox.</p>
            <FaSolidTriangleExclamation />
          </div>
        </div>
      </Show>
      <For each={props.course().chapters}>
        {(c) => {
          const [hidden, setHidden] = createSignal(isHidden);

          return (
            <div
              class="bg-white cursor-pointer dark:bg-black rounded-md m-5 p-2.5 px-5 shadow-md flex flex-col items-center"
              onClick={() => setHidden(!hidden())}
            >
              <div class="text-xl font-medium flex justify-between w-full items-center m-2.5">
                <p>{stripPrefix(c.title)}</p>
                <Show when={hidden()} fallback={<FaSolidChevronUp />}>
                  <FaSolidChevronDown />
                </Show>
              </div>
              <div
                style={
                  !hidden()
                    ? {
                        "max-height":
                          Math.round(c.videos.length * 35).toString() + "px",
                      }
                    : { "max-height": "0" }
                }
                class={
                  hidden()
                    ? "w-full transition-all duration-350 ease-[cubic-bezier(0, 1, 0, 1)] overflow-hidden max-h-0"
                    : "overflow-hidden w-full transition-all duration-350 ease-in-out"
                }
              >
                <For each={c.videos}>
                  {(v) => {
                    return (
                      <div class="w-full my-2.5">
                        <p>{stripPrefix(v.title)}</p>
                      </div>
                    );
                  }}
                </For>
              </div>
            </div>
          );
        }}
      </For>
    </div>
  );
};

export default ImporterAccordion;
