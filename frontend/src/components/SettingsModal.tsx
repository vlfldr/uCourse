import { UIButton, UICheckbox } from "./ui";
import settings, {backgrounds, updateSettings} from "../settings";
import { For } from "solid-js";
import { getSysDarkMode, setSysDarkMode, toggleModal } from "../util";

const SettingsModal = () => {
  return (
    <dialog id="settingsModal" class="modal modal-middle bg-black/80">
      <div class="modal-box w-100vw md:max-h-[85%] md:h-auto md:max-w-md
      bg-white dark:bg-space  text-black dark:text-white">
      <form method="dialog" class="relative">
          <button class="btn btn-sm btn-circle btn-ghost outline-none focus:outline-none active:outline-none absolute right-0 top-0">✕</button>
        </form>
        <p class="text-2xl text-center mb-5">Settings</p>

        <div class="form-control w-full">
          <label class="label">
            <span class="label-text text-black dark:text-white">Background pattern</span>
          </label>
          <select class="select select-bordered  bg-neutral-300 dark:bg-neutral-600 text-black dark:text-white" onInput={(e) => {settings.background = e.target.value}}>
            <option selected>{settings.background}</option>
            <For each={backgrounds}>
              {(opt) => {
                if(opt != settings.background) return <option>{opt}</option>
              }}
            </For>
          </select>

          <div class="mt-5">
          <UICheckbox
              checked={() => settings.darkMode == "dark"}
              setChecked={() => setSysDarkMode(!getSysDarkMode())}
              label="Dark mode"
              reverse={true}
            />
          </div>


        <div class="mt-5">
          <UICheckbox
              checked={() => settings.autoPlay == 1}
              setChecked={() => settings.autoPlay = settings.autoPlay == 1 ? 0 : 1}
              label="Autoplay"
              reverse={true}
            />
          </div>

        </div>


        <div class="flex justify-between mt-5 gap-5">
          <UIButton
            type="tertiary"
            text="Cancel"
            onClick={ (e) => toggleModal("settings", false) }
          />
          <UIButton
            type="primary"
            text="Save"
            enabled={true}
            onClick={ (e) => {
              updateSettings();
              toggleModal("settings", false);
            }}
          />
        </div>
      </div>
    </dialog>
  );
};

export default SettingsModal;
