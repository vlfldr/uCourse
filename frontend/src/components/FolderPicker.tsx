import { Accessor, For, Resource, Setter, Show, createEffect, createSignal } from "solid-js";
import { AiFillFile, AiFillFolder, AiFillUpSquare } from "solid-icons/ai";
import { DirListing } from "../types";

export type FolderPickerProps = {
    rootDir: Resource<DirListing>;
    setSelectedDir: Setter<DirListing>;
    selectedDir: Accessor<DirListing>;
}

const FolderPicker = (props: FolderPickerProps) => {
    // the folder being viewed
    const emptyViewDir: DirListing = props.rootDir()!;
    const [viewDir, setViewDir] = createSignal(emptyViewDir);

    createEffect(() => setViewDir(props.rootDir.latest!))

    const historyStack: DirListing[] = [];
    const traverseDown = (e: Event, f: DirListing) : void => {
        e.preventDefault();
        props.setSelectedDir(new DirListing());

        historyStack.push(viewDir());
        setViewDir(f);
    }
    const traverseUp = (e: Event) : void => {
        e.preventDefault();
        props.setSelectedDir(new DirListing());

        if(historyStack.length == 0) {
            setViewDir(props.rootDir()!);
        }
        else {
            setViewDir(historyStack.pop()!)
        }

        
    }

    const highlightDir = (e: Event, f: DirListing) : void => {
        props.setSelectedDir(f);
    }

    return (
        <>
        <Show when={viewDir().children}>
            <Show when={viewDir().name != props.rootDir()!.name}>
                <div class="bg-white text-space rounded-md p-2.5 md:p-5 flex flex-col items-center justify-start
                        cursor-pointer select-none transition-all hover:-translate-y-1 hover:shadow-md active:shadow-sm text-center" onDblClick={(e) => traverseUp(e)}>
                    <AiFillUpSquare size={48}></AiFillUpSquare>
                    <p>Return</p>
                </div>
            </Show>
            <For each={viewDir().children}>
                {(f) =>
                    <div classList={{'bg-primary text-white': f.name == props.selectedDir().name, 'bg-white text-space': f.name != props.selectedDir().name}}
                        class="rounded-md p-2.5 md:p-5 flex flex-col items-center justify-start
                            cursor-pointer select-none transition-all  hover:-translate-y-1 hover:shadow-md active:shadow-sm text-center" 
                            onClick={(e) => highlightDir(e, f)} 
                            onDblClick={(e) => traverseDown(e, f)}>
                        <Show when={f.type == "file"}
                            fallback={<AiFillFolder size={48}></AiFillFolder>}>
                            <AiFillFile size={48}></AiFillFile>
                        </Show>
                        <p class="break-words">{f.name}</p>
                    </div>
                }
            </For>
        </Show>
        </>
        
    );
}

export default FolderPicker;