import {
    Accessor,
    For,
    Resource,
    Show,
    createEffect,
    createSignal,
    Setter,
    Match,
    Switch,
    onMount,
    createMemo
  } from "solid-js";
  import { Chapter, Course, Video } from "../types";
  import { stripPrefix } from "../util";
  import { FaRegularCircle, FaSolidChevronDown, FaSolidChevronUp, FaSolidCircleCheck, FaSolidCircleHalfStroke } from "solid-icons/fa";
  
  type TheaterAccordionProps = {
    course: Course;
    video: Accessor<Video>;
    setVideoID: Setter<number>;
    chapter: Accessor<Chapter>;
    setChapterID: Setter<number>;
    videoProgMap: Accessor<Map<number, number>>;
  };
  
  const TheaterAccordion = (props: TheaterAccordionProps) => {
    const getHeight = () => {
      const padding = (5 * parseFloat(getComputedStyle(document.documentElement).fontSize));
      const navbarHeight = document.getElementById('navbar')!.clientHeight;
      return window.innerHeight - padding - navbarHeight
    }
    const handleResize = () => {
      setHeight(getHeight());
    }

    const vidProg = (v: Video) => props.videoProgMap().get(v.id) ?? v.watchedSec;
    const [height, setHeight] = createSignal(getHeight())

    onMount(() => window.addEventListener('resize', handleResize))

    // caching here prevents accordion breaking while new chap/vid is being recomputed in parent
    // these are signals not resources so we can't use .latest
    let cachedChapter: Chapter;
    let cachedVideo: Video;
    createEffect(() => cachedChapter = props.chapter());
    createEffect(() => cachedVideo = props.video());

    const chapter = createMemo(() => props.chapter() || cachedChapter)
    const video = createMemo(() => props.video() || cachedVideo)

    return (
        <div
          style={"max-height: " + height() + "px"}
          class="overflow-y-scroll lg:min-w-md lg:max-w-md basis-1/3 rounded-md shadow-md h-min bg-neutral-300 dark:bg-neutral-600 text-black dark:text-white"
        >
  
          <For each={props.course.chapters}>
            {(c) => {
              const [hidden, setHidden] = createSignal(c != chapter());
  
              createEffect(() => setHidden(c != chapter()));
  
              return (
                <div
                  class="bg-white text-left dark:bg-black rounded-md m-5 overflow-hidden shadow-md flex flex-col items-center
                  "
                  
                >
                  <div class="text-xl cursor-pointer font-medium flex p-5 justify-between w-full items-center"
                    onClick={() => setHidden(!hidden())}>
                    <p class="transition-all"
                      classList={{"text-violet-700": c.id == chapter().id}}
                    >
                      {stripPrefix(c.title)}
                    </p>
                    <Show when={hidden()} fallback={<FaSolidChevronUp />}>
                      <FaSolidChevronDown />
                    </Show>
                  </div>
                  <div
                    style={
                      !hidden()
                        ? {
                            "max-height":
                              (Math.round(c.videos.length * 44) + 20).toString() + "px",
                          }
                        : { "max-height": "0" }
                    }
                    class={
                      hidden()
                        ? "w-full transition-all duration-350 ease-[cubic-bezier(0, 1, 0, 1)] overflow-hidden max-h-0"
                        : "overflow-hidden w-full bg-white-700 dark:bg-black-300 py-2.5 transition-all duration-350 ease-in-out"
                    }
                  >
                    <For each={c.videos}>
                      {(v) => {
                        return (
                          <div class="w-full flex items-center text-left p-2.5 px-5  justify-between"
                          onClick={() => {
                            if(video().chapterID != v.chapterID) {
                              props.setChapterID(v.chapterID);
                            }
                            props.setVideoID(v.id);
                          }}>
                            <div class="flex items-center gap-2.5">
                            <Switch fallback={<FaRegularCircle />}>
                                <Match
                                    when={
                                    v.lengthSec - vidProg(v) <=
                                    Math.floor(v.lengthSec * 0.15)
                                    }
                                >
                                    <FaSolidCircleCheck />
                                </Match>
                                <Match when={vidProg(v) > 5}>
                                    <FaSolidCircleHalfStroke />
                                </Match>
                                
                            </Switch>
                            <p
                            classList={{"text-violet-700": v.id == video().id}}
                            class="cursor-pointer trasnsition-all"
                            >
                              {stripPrefix(v.title)}
                            </p>
                            </div>
                            <p>
                              {Math.floor(v.lengthSec / 60)}:
                              {Math.round(v.lengthSec % 60)
                                .toString()
                                .padStart(2, "0")}
                            </p>
                          </div>
                        );
                      }}
                    </For>
                  </div>
                </div>
              );
            }}
          </For>
        </div>
    );
  };
  
  export default TheaterAccordion;
  