import { Course } from "../types";
import { deleteCourse, getCourseProgress, getCourseThumbnail } from "../api";
import {
  createSignal,
  createResource,
  Show,
  createMemo,
  Setter,
} from "solid-js";
import {
  FaSolidBookBookmark,
  FaSolidEye,
  FaSolidPenToSquare,
  FaSolidTrashCan,
  FaSolidVideo,
} from "solid-icons/fa";
import { A } from "@solidjs/router";
import { toggleModal } from "../util";

type CourseCardProps = {
  course: Course;
  fetchCourses: () => Course[] | Promise<Course[] | undefined> | null | undefined;
  setEditorCourseId: Setter<string>;
};

const CourseCard = (props: CourseCardProps) => {
  const c: Course = props.course;

  const [course, getCourse] = createSignal(props.course);
  const [courseImage] = createResource(course, getCourseThumbnail);
  const [courseProgress] = createResource(course, getCourseProgress);

  const percentageComplete = createMemo(() => {
    if (courseProgress.loading) return;

    let complete =
      (courseProgress()!.totalWatchtime / courseProgress()!.totalRuntime) * 100;
    if (complete > 95) complete = 100;

    return complete.toFixed(1) + "%";
  });

  const timeRemaining = createMemo(() => {
    if (courseProgress.loading) return;

    let res = "";
    let remSec =
      courseProgress()!.totalRuntime - courseProgress()!.totalWatchtime;

    if (remSec > 3600) {
      let remHours = Math.floor(remSec / 3600);

      res += remHours.toString();
      res += remHours > 1 ? " hours, " : " hour, ";

      remSec -= remHours * 3600;
    }

    const remMin = Math.floor(remSec / 60);

    res += remMin + " minutes remaining";

    return res;
  });

  return (
    <div
      class="block rounded-lg max-w-[600px] bg-white-800 shadow-md dark:bg-neutral-700
      transition-all duration-200 hover:-translate-y-1 transform-gpu will-change-transform hover:shadow-lg active:shadow-sm active:translate-y-0"
    >
      <div class="relative overflow-hidden bg-cover bg-no-repeat">
        <Show when={!courseImage.loading}>
          <A class="h-min" href={"/watch/" + props.course.id.toString()}>
            <img
              class="rounded-t-lg w-[600px] aspect-video"
              src={courseImage()}
              alt={course().title + " thumbnail"}
            />
            <div class="absolute bottom-0 left-0 right-0 top-0 h-full w-full overflow-hidden bg-[hsla(0,0%,98%,0.15)] bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-100"></div>
          </A>
        </Show>
      </div>

      <div class="flex flex-col gap-5 p-5">
        <div>
          <h5 class="text-xl font-300 leading-tight text-neutral-800 dark:text-neutral-100">
            {course().title}
          </h5>
        </div>
        <p class="text-base text-neutral-600 break dark:text-neutral-200">
          {course().description}
        </p>

        <div class="flex h-min gap-5 items-center justify-start w-full font-bold tracking-tight text-neutral-300 dark:text-neutral-500/80">
          <Show when={!courseProgress.loading}>
            <div
              class="flex items-center gap-2.5 h-min"
              title={courseProgress()!.numVideos + " videos"}
            >
              <FaSolidVideo />
              <p>{courseProgress()!.numVideos}</p>
            </div>

            <div
              class="flex items-center gap-2.5"
              title={courseProgress()!.numChapters + " chapters"}
            >
              <FaSolidBookBookmark />
              <p>{courseProgress()!.numChapters}</p>
            </div>

            <div class="flex items-center gap-2.5" title={timeRemaining()}>
              <FaSolidEye />
              <p>{percentageComplete()}</p>
            </div>

            <div class="ml-auto flex items-center gap-2.5">
              <div title="Edit">
                <FaSolidPenToSquare
                  class="cursor-pointer transition-all duration-200 hover:text-black dark:hover:text-white"
                  onClick={() => {
                    props.setEditorCourseId(props.course.id.toString());
                    toggleModal("editor", true);
                  }}
                ></FaSolidPenToSquare>
              </div>

              <div title="Delete">
                <FaSolidTrashCan
                  class="cursor-pointer transition-all duration-200 hover:text-red-500"
                  onClick={async (e) => {
                    await deleteCourse(props.course.id);
                    props.fetchCourses();
                  }}
                ></FaSolidTrashCan>
              </div>
            </div>
          </Show>
        </div>
      </div>
    </div>
  );
};

export default CourseCard;
