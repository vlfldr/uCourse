import { Accessor, Setter, Show, createResource, createSignal, onCleanup } from "solid-js";
import { Course, DirListing } from "../types";
import { listMediaDir } from "../api";
import { dirListingToCourse, toggleModal } from "../util";
import FolderPicker from "./FolderPicker";
import { UIButton, UICheckbox } from "./ui";
import { FaSolidArrowRotateRight } from "solid-icons/fa";

export type CourseImporterProps = {
  selectCourse: Setter<Course>;
};

const CourseImporter = (props: CourseImporterProps) => {
  const [dirTree, {mutate, refetch}] = createResource(listMediaDir);

  const [selectedDir, setSelectedDir] = createSignal(new DirListing());
  const [groupBySubfolder, setGroupBySubfolder] = createSignal<boolean>(true);

  return (
    <>
      <p class="text-2xl text-center">Import Course</p>

      <div
        class="bg-quartz_light dark:bg-quartz_dark h-full my-5 overflow-y-scroll rounded-md
                    shadow-inner flex flex-col md:grid md:auto-rows-min md:grid-cols-3 lg:grid-cols-4 gap-2.5 md:gap-5 p-2.5 md:p-5 max-h-lg"
      >
        <Show when={dirTree.latest !== undefined}>
          <FolderPicker
            rootDir={dirTree}
            setSelectedDir={setSelectedDir}
            selectedDir={selectedDir}
          ></FolderPicker>
        </Show>

        
      </div>

      <div class="mb-5 flex w-full">
            <UICheckbox
              checked={groupBySubfolder}
              setChecked={setGroupBySubfolder}
              label="Chapters separated by directory"
            />
          </div>

      <div class="flex justify-end mt-2 gap-5">
        <div class="mr-auto">
        <UIButton
          type="tertiary"
          text="Cancel"
          onClick={(e) => toggleModal("editor", false) }
        />
        </div>
        <UIButton
          type="secondary"
          text="Refresh"
          enabled={true}
          onClick={() => {
            refetch();

            // show toast with Portal
          }}
        />
        <UIButton
          type="primary"
          text="Continue"
          enabled={selectedDir().name != ""}
          onClick={(e) => props.selectCourse(dirListingToCourse(selectedDir(), groupBySubfolder()))}
        />
      </div>
    </>
  );
};

export default CourseImporter;
