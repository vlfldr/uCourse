import {
  Accessor,
  Resource,
  Show,
  createEffect,
  createMemo,
  createResource,
  createSignal,
  onCleanup,
  onMount,
} from "solid-js";
import { createStore } from "solid-js/store";
import { apiURL, getFullCourse, updateVideoWatchedSec } from "../api";

import videojs from "video.js";
import Player from "video.js/dist/types/player";
import "video.js/dist/video-js.css";
import "@videojs/themes/sea/index.css";
import {
  Params,
  useParams,
} from "@solidjs/router";
import { Chapter, Course, Video } from "../types";
import { stripPrefix } from "../util";
import TheaterAccordion from "./TheaterAccordion";
import { Portal } from "solid-js/web";
import settings, { updateSettings } from '../settings'

const Theater = () => {
  let player: Player;
  let params: Params = useParams();

  const [videoProgMap, setVideoProgMap] = createSignal(new Map<number, number>(), { equals: false });

  const [courseRsc] = createResource(() => params.id, getFullCourse);
  const [course, setCourse] = createStore(new Course());
  const [chapterID, setChapterID] = createSignal(-1);
  const [videoID, setVideoID] = createSignal(-1);

  const [autoplay, setAutoplay] = createSignal(settings.autoPlay);

  const saveProgress = () => updateVideoWatchedSec([...videoProgMap().keys()], [...videoProgMap().values()]);

  onMount(() => {
    player = videojs("video-js-element", {});

    // custom autoplay button
    let autoplayBtn = player.getChild('ControlBar')?.addChild("button");
    autoplayBtn!.controlText('Autoplay');
    player.getChild('ControlBar')?.el().insertBefore(autoplayBtn!.el(), player.getChild('ControlBar')!.getChild('FullscreenToggle')!.el());
    autoplayBtn!.el().innerHTML=`<input type="checkbox" class="mt-0.5 toggle toggle-xs" />`
    if(autoplay() === 1) {
      autoplayBtn!.el().innerHTML=`<input type="checkbox" class="mt-0.5 toggle toggle-xs" checked />`;
    }
    autoplayBtn!.el().addEventListener('click', () => setAutoplay(autoplay() === 1 ? 0 : 1));

    // save video progress to db when user navigates away or closes page
    window.addEventListener('beforeunload', saveProgress);
    window.addEventListener('close', saveProgress);
  });

  onCleanup(() => {
    if (player) player.dispose();

    saveProgress();
    window.removeEventListener('beforeunload', saveProgress);
    window.removeEventListener('close', saveProgress);
  });

  // update settings when autoplay slider toggled
  createEffect(() => {
    settings.autoPlay = autoplay();
    updateSettings();
  });

  // feels weird to autoplay when clicking on a video manually
  // this will be set to true when video ends
  let shouldAutoplay = false;

  // set Chapter when ID changes
  const chapter = createMemo(
    () => course.chapters.find((c) => c.id == chapterID())!
  );
  
  // set Video when ID changes
  const video = createMemo(
    () =>
      course.chapters
        .find((c) => c.id == chapterID())
        ?.videos.find((v) => v.id == videoID())!
  );

  // set Course when resource is refetched
  // set initial chapter + video
  createEffect(() => {
    if (!courseRsc.loading) {
      setCourse(courseRsc.latest!);
      let chap = 0;

      // skip chapters with no videos
      while (course.chapters[chap].videos.length == 0) {
        chap += 1;
      }

      setChapterID(course.chapters[chap].id);
      setVideoID(course.chapters[chap].videos[0].id);
      setCourse((crs) => ({ ...crs }));
    }
  });

  // set video stream URL when videoID changes
  const videoURL = createMemo(() => {
    return apiURL + "video/" + videoID() + "/stream";
  });

  // reload video src when videoID() changes
  createEffect(() => {
    if (videoID() !== -1 && player) {
      player.load();
    }
  });

  return (
    <div class="h-full w-full grow flex flex-col p-5 lg:p-10">
      <Show when={!courseRsc.loading && video()}>
        <Portal mount={document.getElementById("title-portal")!}>
          <div class="flex flex-row grow items-center justify-evenly text-center">
            <div class="text-xl text-neutral-500 break-words overflow-hidden max-h-[1.75rem]">
              {course.title}
            </div>
            <div class="text-xl text-black dark:text-white text-ellipsis text-wrap max-h-[1.75rem] break-words overflow-hidden">
              {stripPrefix(video()!.title)}
            </div>
          </div>
        </Portal>
      </Show>

      <div class="flex flex-col-reverse justify-end grow lg:flex-row gap-5 lg:gap-10">
        <TheaterAccordion
          course={course}
          video={video}
          setVideoID={setVideoID}
          chapter={chapter}
          setChapterID={setChapterID}
          videoProgMap={videoProgMap}
        />

        <div class="max-h-full w-full">
          <div data-vjs-player>
            <video
              controls
              preload="none"
              id="video-js-element"
              onEnded={() => {
                if(autoplay()) {
                  // if at end of chapter
                  if (videoID() == chapter().videos.at(-1)!.id) {
                    // do nothing if last chapter of course
                    if (chapterID() == course.chapters.at(-1)!.id) return;

                    // go to next chapter
                    setChapterID(course.chapters[chapter().sortIndex + 1].id);
                    setVideoID(chapter().videos[0].id);
                  } else {
                    // go to next video
                    setVideoID(chapter().videos[video().sortIndex + 1].id);
                  }

                  shouldAutoplay = true;
                }
              }}
              onLoadedData={() => {
                player.currentTime(videoProgMap().get(videoID()) || video().watchedSec);
                if(shouldAutoplay && autoplay()) {
                  player.play();
                  shouldAutoplay = false;
                } 
              }}
              onTimeUpdate={() => {
                if(player && !player.paused()) {
                videoProgMap().set(videoID(), player.currentTime()!);
                setVideoProgMap(videoProgMap());
                }
              }}
              class="video-js vjs-fluid vjs-big-play-centered vjs-show-big-play-button-on-pause"
            >
              <Show when={videoID() !== -1}>
                <source src={videoURL()} type="video/mp4" />
              </Show>
            </video>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Theater;
