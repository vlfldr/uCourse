import {
  Setter,
  createEffect,
  createSignal,
  onMount,
} from "solid-js";
import { FiSun } from "solid-icons/fi";
import { FaSolidMoon, FaSolidSliders } from "solid-icons/fa";
import { UIIconButton } from "./ui";
import { A } from "@solidjs/router";
import { getSysDarkMode, setSysDarkMode, toggleModal } from "../util";

import logoImg from "../assets/thumbnail_placeholder.png"

const Navbar = () => {
  const [darkMode, setDarkMode] = createSignal(getSysDarkMode());

  // apply dark/light mode to document and root element on swap
  createEffect(() => {
    
    setSysDarkMode(darkMode())
  })


  return (
    <nav
      id="navbar"
      class="relative flex w-full flex-wrap items-center justify-start bg-white-800 
      text-neutral-500 shadow-lg hover:text-neutral-700 focus:text-neutral-700 dark:bg-neutral-600 py-4"
    >
      <div class="flex w-full flex-wrap items-center justify-start">
        <div>
          <A href="/"
            class="mx-5 my-1 w-10 h-10 cursor-pointer flex items-center text-neutral-900 hover:text-neutral-900 focus:text-neutral-900 mb-0 mt-0"
          >
            <img
              src={logoImg}
              width="40"
              height="40"
              alt="uCourse Logo"
              loading="lazy"
            />
          </A>
        </div>
        <A href="/"
          class="text-space-600 cursor-pointer text-xl transition-all duration-200 hover:text-neutral-700 dark:text-neutral-200 dark:hover:text-white"
          aria-current="page"
          data-te-nav-link-ref
        >
          uCourse
        </A>

        <div id="title-portal" class="max-h-2rem max-w-[80%] grow mx-auto"></div>


          <div class="flex ml-auto items-center justify-normal mt-0.5 mr-2.5">
            <UIIconButton icon={darkMode() ? <FaSolidMoon size={22} /> : <FiSun size={22} />} 
                    onClick={() => setDarkMode(!darkMode())}/>
            <UIIconButton
              icon={<FaSolidSliders size={22} />}
              onClick={() => toggleModal("settings", true) }
            />
          </div>
        </div>
    </nav>
  );
};

export default Navbar;
