import { For, Show } from "solid-js/web";
import { Course } from "../types";
import { UIFloatingIconButton, UISpinner } from "./ui";
import { AiOutlinePlus } from "solid-icons/ai";
import CourseCard from "./CourseCard";
import { Resource, Setter } from "solid-js";
import { toggleModal } from '../util';

type CourseListProps = {
  courses: Resource<Course[]>;
  fetchCourses: () => Course[] | Promise<Course[] | undefined> | null | undefined;
  setEditorCourseId: Setter<string>;
};

const CourseList = (props: CourseListProps) => {
  return (
    <>
      <UIFloatingIconButton
      title="Import"
      onClick={() => toggleModal("editor", true) }
        icon={
          <AiOutlinePlus
            size={22}
          />
        }
      />
      <Show when={!props.courses.loading && props.courses()!.length == 0}>
        <div class="w-full h-full flex grow justify-center text-center items-center">
          <p class="text-4xl text-dogwood-700 dark:text-violet-300">
            Import a course to get started
          </p>
        </div>
      </Show>

      <Show when={props.courses.loading}>
        <div
          class="flex grow 
          flex-wrap justify-center items-center  h-full overflow-scroll-y"
        >
          <UISpinner />
        </div>
      </Show>

      <Show when={!props.courses.loading && props.courses()!.length != 0}>
        <div
          class="flex flex-col items-center gap-10 p-10 h-full w-full overflow-scroll-y
            lg:flex lg:flex-row lg:justify-center lg:flex-wrap grow"
        >
          <For each={props.courses()}>
            {(c) => (
                <CourseCard course={c} 
                  fetchCourses={props.fetchCourses}
                  setEditorCourseId={props.setEditorCourseId} />
            )}
          </For>
        </div>
      </Show>
    </>
  );
};

export default CourseList;
