import { Accessor, Setter, Show, createEffect, createResource, createSignal, onCleanup } from "solid-js";
import { Course } from "../types";
import { createCourse, getFullCourse, updateCourse, updateCourseTitles } from "../api";
import { UIButton, UICheckbox, UISpinner } from "./ui";
import CourseImporter from "./CourseImporter";
import ImporterAccordion from "./ImporterAccordion";
import { toggleModal } from "../util";
import { FaSolidArrowRotateRight, FaSolidCircleInfo, FaSolidInfo, FaSolidQuestion } from "solid-icons/fa";

// CourseEditor is passed a new Course from CourseImporter or an existing Course from CourseList.

export type CourseEditorProps = {
  courseID?: Accessor<string>;
  setCourseId?: Setter<string>;
  refetch: () => Course[] | Promise<Course[] | undefined> | null | undefined;
};

const dummy: Course = new Course();

const CourseEditor = (props: CourseEditorProps) => {
  const [selectedCourse, selectCourse] = createSignal(dummy);
  const [awaitingResponse, setAwaitingResponse] = createSignal(false);
  const [delimiter, setDelimiter] = createSignal("");
  const [passedCourse] = createResource(props.courseID, getFullCourse);

  createEffect(() => {
    if(props.courseID && props.courseID() !== "-1") {
      selectCourse(passedCourse()!)
    }
  })

  const handleClose = () => {
    if(props.courseID)  props.setCourseId!("-1");
    setDelimiter("");
    
    setTimeout(() => selectCourse(dummy), 300);
  }

  const handleConfirm = async() => {
    setAwaitingResponse(true);

    let id: number;

    if(!selectedCourse() || selectedCourse() === dummy || selectedCourse().id === -1){
      id = (await createCourse(selectedCourse())).id;
    } else {
      id = (await updateCourse(selectedCourse())).id;
    }

    if(delimiter().trim() !== "") {
      await updateCourseTitles(id, delimiter());
    }

    if(props.courseID)  props.setCourseId!("-1");   
    setDelimiter(""); 
    setAwaitingResponse(false);
    toggleModal("editor", false);
    setTimeout(props.refetch, 100);
  }

  return (
    <dialog id="editorModal" class="modal modal-middle bg-black/70" 
      onClose={handleClose}>
      <div
        class="modal-box w-100vw md:max-h-[85%] md:h-auto md:max-w-3xl
      bg-white dark:bg-space  text-black dark:text-white"
      >
        <form method="dialog" class="relative">
          <button class="btn btn-sm btn-circle btn-ghost outline-none focus:outline-none active:outline-none absolute right-0 top-0">
            ✕
          </button>
        </form>
        <Show
          when={selectedCourse() && selectedCourse() != dummy}
          fallback={<CourseImporter selectCourse={selectCourse} />}
        >
          <p class="text-2xl text-center">Edit course</p>

          <div class="form-control w-full my-5">
            <label class="label">
              <span class="label-text text-black dark:text-white">Title</span>
            </label>
            <input
              type="text"
              value={selectedCourse().title}
              onInput={(e) => {
                let sc = selectedCourse();
                sc.title = e.currentTarget.value;
                selectCourse(sc);
              }}
              class="input input-bordered w-full bg-neutral-300 dark:bg-neutral-600 text-black dark:text-white"
            />
          </div>

          <div class="form-control mb-5">
            <label class="label">
              <span class="label-text text-black dark:text-white">
                Description (optional)
              </span>
            </label>
            <textarea
              value={selectedCourse().description}
              onInput={(e) => {
                let sc = selectedCourse();
                sc.description = e.currentTarget.value;
                selectCourse(sc);
              }}
              class="textarea textarea-bordered bg-neutral-300 dark:bg-neutral-600 text-black dark:text-white"
              aria-label="Description"
            ></textarea>
          </div>

          <div class="form-control mb-5">
            <label class="label">
              <span class="label-text flex items-center gap-2.5 text-black dark:text-white"
                title="The specified character will be replaced with spaces in video and chapter titles. Examples: _ - .\n\nLeave this alone if your files are named correctly with spaces.">
                Word delimiter (optional)
                <FaSolidQuestion />
              </span>
            </label>
            <input
              value={delimiter()}
              onInput={(e) => {
                setDelimiter(e.currentTarget.value);
              }}
              class="textarea textarea-bordered bg-neutral-300 dark:bg-neutral-600 text-black dark:text-white"
              aria-label="Word delimiter"
            ></input>
          </div>

          <ImporterAccordion course={selectedCourse} />

          <div class="flex justify-end gap-5">
            <div class="ml-0 mr-auto">
              <UIButton
                text="Cancel"
                type="tertiary"
                onClick={() => {
                  toggleModal("editor", false);
                  handleClose();
                }}
              />
            </div>
            <UIButton
              enabled={true}
              text="Return"
              type="secondary"
              onClick={(e) => selectCourse(dummy)}
            />
            <div class="relative">
              <UIButton
                enabled={!awaitingResponse() && selectedCourse() && selectedCourse().chapters.length > 0}
                text={awaitingResponse() ? "Working..." : "Confirm"}
                type="primary"
                onClick={() => {
                  handleConfirm();
                }}
              />
            </div>
          </div>
        </Show>
      </div>
    </dialog>
  );
};

export default CourseEditor;
