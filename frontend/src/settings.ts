import { getSettings, setSettings } from "./api";

export const backgrounds = ['Circuits', 'Rain', 'Intersections'];

let settings = await getSettings()

export const updateSettings = async () => {
    return await setSettings(settings);
}

export default settings