import { defineConfig } from 'vite';
import UnoCSS from 'unocss/vite';
import presetUno from '@unocss/preset-uno';
import solidPlugin from 'vite-plugin-solid';
import { compression } from 'vite-plugin-compression2'
import { presetDaisy } from 'unocss-preset-daisy';
import { VitePWA } from 'vite-plugin-pwa';

export default defineConfig({
  plugins: [
    solidPlugin(),
    UnoCSS({
      presets: [
        presetUno(), 
        presetDaisy()
      ],
    }),
    compression(),
    compression({algorithm: 'brotliCompress'})
  ],
  server: {
    port: 3000
  },
  build: {
    target: 'esnext',
    minify: 'esbuild',
    cssMinify: 'esbuild'
  },
  optimizeDeps: {
    extensions: ["jsx","tsx"],
  }
});
