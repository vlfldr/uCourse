<div align="center">
  <h2>uCourse</h2>
  <img src="frontend/src/assets/android-chrome-192x192.png" alt="uCourse logo"/>
  <p>A tool to manage and watch educational videos.</p>
  <img src="doc/screenshot_main.png" alt="Course list screenshot"/>
  <img src="doc/screenshot_video.png" alt="Video player screenshot"/>
</div>

### This readme is a pratctial quickstart to get uCourse running. check out [this page](https://vlfldr.com/projects/ucourse) for a longer-form retrospective and insights into working with the tech stack.

## Table of Contents
<!-- TOC -->
- [About](#about)
- [Folder Structure](#folder-structure)
- [Install](#install)
- [Features](#features)
- [Planned Features](#planned-features)
- [Built With](#built-with)
<!-- /TOC -->

## About

So you've legally downloaded some video courses from your favorite online learning platform. Dragging the folder into MPV or VLC works, but you find yourself missing out on features like library management, progress tracking, streaming content on the go, etc. Maybe it's getting annoying to wireguard into your home server or try to wrangle the courses into Jellyfin/Plex. Enter uCourse!

uCourse is a self-hosted video course library. It's built from the ground up to be very fast and lightweight. To accomplish this, the project uses:
- [Bun](https://bun.sh) - ultrafast JS runtime and SQLite driver.
- [Hono](https://hono.dev) - API framework *16x* faster than Express.
- [Drizzle ORM](https://orm.drizzle.team/) - lightweight TypeScript ORM *100x* faster than Prisma.
- [SolidJS](https://www.solidjs.com/) - tiny, performant, fine grained reactivity framework.
- [UnoCSS](https://unocss.dev/) - lightweight on-demand atomic CSS engine.
- [DaisyUI](https://daisyui.com/) - consistent, polished UI components.

<div align="center">
  <img src="doc/lighthouse.gif" alt="Lighthouse score"/>
</div>

## Install

# THIS IS NOT DONE! NEED TO PUBLISH TO HUB!
Currently Docker is the only supported way to run uCourse. 

1. Copy the example below to docker-compose.yml
1. **Replace /home/leary/courses with the path to your course videos.**
1. Run `docker compose up -d`
1. Navigate to http://localhost:9333 to get started!
```
version: "3.1"

services:
  ucourse-api:
    image: ucourse-api
    build: ./backend
    container_name: ucourse-api
    hostname: ucourse-api
    volumes:
      - /home/leary/courses:/home/bun/app/courses

  ucourse:
    image: ucourse
    build: ./frontend
    container_name: ucourse
    hostname: ucourse
    ports:
      - "9333:80"
    depends_on:
      - "ucourse-api"
```

## Folder structure
In the folder you specified above, each sub-folder should contain a separate course. These sub-folders can either be farther divided into chapter sub-folders:

    courses
    |- Advanced JavaScript
      |- 01 Introduction
        |- 01 What to expect.mp4
        |- 02 Environment setup.mp4
        ...
      |- 02 Variables
        |- 01 Primitive types.mp4
        |- 02 Strings.mp4
        ...
      ...

...or contain a flat collection of videos with no chapters:

    courses
    |- WebAssembly - Beginner to Advanced
      |- 001 Why You Should Watch This Course.mp4
      |- 002 How to Watch This Course.mp4
      |- 003 Goals of WebAssembly.mp4
      ...

Things to keep in mind:
- **Currently only .mp4 videos are supported**
- **Each chapter folder and each video file must be sequentially numbered**

Sequential prefixes are required to determine playback order. This is a common naming scheme - most courses available for download from your totally legal sources will work out of the box.

## Features
- Very fast
- Track your pgoress
- Import local folders
- Stream course videos from a home server to any internet connected web browser

## Planned Features
- Drag and drop playback order editor
- Subtitle support
- Download files for offline playback